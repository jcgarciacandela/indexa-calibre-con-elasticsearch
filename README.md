(Proyecto en desarrollo!)
Proyecto simple para probar el uso de Spring Boot / ElasticSearch. 

La idea es a partir de una biblioteca Calibre (https://calibre-ebook.com/)
y una instancia ElasticSearch (https://www.elastic.co/downloads/elasticsearch-oss)
indexar todo el contenido de la biblioteca.

Se centra en backend, no me interesa especialmente desarrollar frontend REST
complejos, al menos no hasta completar el core.

La configuración reside en application.properties, bien el de main/resources o
aportándolo externamente:\
https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html#boot-features-external-config-application-property-files

elasticsearch.path = (ruta relasticsearch)\
calibre.path = (ruta biblioteca calibre)\
max.books.indexall = (número de libros a indexar con /indexall)

/indexbook (@param id), indexa el libro con ID indicado (ID de calibre)\
/indexall   indexa max.books.indexall libros a partir del último ID indexado

- Sin endpoints de búsqueda por ahora.
- Puede incorporarse stemming y stopwords modificando el filtro en setting.json,\
  o creando un nuevo analyzer.
  
![cap_01.jpg](http://imagehost.cc/images/2019/03/25/cap_01.jpg)