package com.jcarlos.indexacalibre.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.elasticsearch.annotations.*;

import java.util.List;

@Data
@Document(indexName = "calibre", type = "book")
@Setting(settingPath = "/settings/settings.json")
@Mapping(mappingPath = "/mappings/mappings.json")
public class Book {

  @Id
  @Field(type=FieldType.Keyword)
  // Duplicated (id/idCalibre) for simplicity, debug and educational purposes, normally it would be generated automatically.
  // ElasticSearch uses String type id, aggregations on nor-numeric fields is inefficient.
  private String id;

  @Field(type=FieldType.Integer) // For aggregations (max)
  private int idCalibre;

  @Field(type = FieldType.Keyword)
  private String title;

  @Field(type = FieldType.Keyword)
  private String path;

  @Field(type = FieldType.Nested, includeInParent = true)
  private List<String> authors;

  @Field(type = FieldType.Nested, includeInParent = true)
  private List<String> tags;

  @Field(type = FieldType.Keyword)
  private String contentFileName;

  @Field(type = FieldType.Long)
  private Long contentFileSize;

  @Field(type = FieldType.Integer)
  private int contentSize;

  @Field(type = FieldType.Text, analyzer = "calibre_text", searchAnalyzer = "calibre_text")
  private String content;

  public void setId(String id) {
    this.id=id;
    this.idCalibre=Integer.parseInt(id);
  }

  @Transient
  @JsonProperty(access = JsonProperty.Access.READ_ONLY)
  String[] hightLightFragments;

}