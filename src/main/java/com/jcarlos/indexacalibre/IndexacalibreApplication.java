package com.jcarlos.indexacalibre;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IndexacalibreApplication {

  public static void main(String[] args) {
    SpringApplication.run(IndexacalibreApplication.class, args);
  }

}
