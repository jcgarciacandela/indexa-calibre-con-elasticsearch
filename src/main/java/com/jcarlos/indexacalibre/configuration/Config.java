package com.jcarlos.indexacalibre.configuration;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

@Component
@Configuration
@EnableElasticsearchRepositories(basePackages = "com.jcarlos.indexacalibre.repositories")
@ComponentScan(basePackages = {"com.jcarlos.indexacalibre.services"})
public class Config {


  @Value("${elasticsearch.path}")
  private String elasticsearchHome;

  @Value("${elasticsearch.cluster.name:elasticsearch}")
  private String clusterName;

  @Bean
  public Client client() {
    Settings elasticsearchSettings = Settings.builder()
            .put("client.transport.sniff", true)
            .put("path.home", elasticsearchHome)
            .put("cluster.name", clusterName).build();
    TransportClient client = new PreBuiltTransportClient(elasticsearchSettings);
    try {
      client.addTransportAddress(new TransportAddress(InetAddress.getByName("127.0.0.1"), 9300));
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }

    return client;
  }


}