package com.jcarlos.indexacalibre.repositories;

import com.jcarlos.indexacalibre.model.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends ElasticsearchRepository<Book, String> {

}