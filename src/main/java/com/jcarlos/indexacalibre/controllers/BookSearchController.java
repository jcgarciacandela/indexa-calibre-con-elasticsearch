package com.jcarlos.indexacalibre.controllers;

import com.jcarlos.indexacalibre.services.BookSearchResult;
import com.jcarlos.indexacalibre.services.BookSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BookSearchController {

  private BookSearchService bookSearchService;

  @Autowired
  void setBookSearchService(BookSearchService bookSearchService) {
    this.bookSearchService = bookSearchService;
  }

  @RequestMapping(value = "//search")
  public BookSearchResult search(@RequestParam MultiValueMap<String, String> params) {

    return bookSearchService.searchBooksContent("quintral","exact_phrase",false,10, 0);

  }


}
