package com.jcarlos.indexacalibre.controllers;

import com.jcarlos.indexacalibre.model.Book;
import com.jcarlos.indexacalibre.services.BookIndexService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;

@RestController
public class BookIndexController {

  private BookIndexService bookIndexService;

  @Value("${max.books.indexall:100}")
  int maxBooksIndexAll;

  @Autowired
  void setBookIndexService(BookIndexService bookIndexService) {
    this.bookIndexService = bookIndexService;
  }
  private static final Log log = LogFactory.getLog(BookIndexController.class);

  @RequestMapping(value = "/indexbook")
  public Book indexBook(@RequestParam("id") int  id) {
    return bookIndexService.indexBook(id);
  }

  @RequestMapping(value = "/indexall")
  public HashMap<String,Integer> indexaTodo(@RequestParam(value = "fromlastindexed", defaultValue = "true") boolean fromLastIndexed) {
    try {
      HashMap<String, Integer> result = bookIndexService.indexAllBooks(fromLastIndexed, maxBooksIndexAll);
      return result;
    } catch (IOException ex) {
      log.error(ex);
      ex.printStackTrace();
    }

    return null;
  }


}
