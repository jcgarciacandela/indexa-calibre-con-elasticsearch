package com.jcarlos.indexacalibre.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcarlos.indexacalibre.model.Book;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchResultMapper;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.core.query.SourceFilter;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class BookSearchService {

  private static final Log log = LogFactory.getLog(BookSearchService.class);

  @Value("${highlight.max.fragments:100}")
  private int HIGHLIGHT_MAX_FRAGMENTS;

  @Value("${highlight.fragment.size:100}")
  private int HIGHLIGHT_FRAGMENT_SIZE;


  private ElasticsearchOperations elasticSearchTemplate;

  public enum tipoBusqueda {
    some_terms,
    all_terms,
    exact_phrase
  }

  @Autowired
  void setElasticSearchTemplate(ElasticsearchOperations elasticSearchTemplate) {
    this.elasticSearchTemplate = elasticSearchTemplate;
  }


  public BookSearchResult searchBooksContent(String text, String type, boolean fuzzy, int recordsPerPage, int pageNumber) {

    String[] terms = text.split("\\s");

    if (terms.length == 0) {
      terms = new String[]{""};
    }

    int minimumTermsMatched = 1;
    Fuzziness fuzziness = fuzzy ? Fuzziness.AUTO : Fuzziness.ZERO;

    if (type.equalsIgnoreCase(tipoBusqueda.all_terms.toString())) {
      minimumTermsMatched = terms.length;
    }

    SearchQuery searchQuery;
    SourceFilter sourceFilter = new FetchSourceFilter(null, new String[]{"content"});

    QueryBuilder queryBuilder;
    if (type.equalsIgnoreCase(tipoBusqueda.exact_phrase.toString())) {

      queryBuilder = QueryBuilders.multiMatchQuery(text)
              .field("content")
              .analyzer("calibre_text")
              .fuzziness(fuzziness)
              .fuzzyTranspositions(true)
              .type(MultiMatchQueryBuilder.Type.PHRASE);
    } else {
      queryBuilder = new BoolQueryBuilder().minimumShouldMatch(minimumTermsMatched);
      for (String term : terms) {
        queryBuilder = ((BoolQueryBuilder) queryBuilder)
                .should(QueryBuilders.multiMatchQuery(term, "content")
                        .operator(Operator.AND)
                        .fuzziness(fuzziness)
                        .fuzzyTranspositions(true)
                        .analyzer("calibre_text")
                );
      }


    }

    HighlightBuilder highlightBuilder = new HighlightBuilder()
            .field("content")
            .numOfFragments(HIGHLIGHT_MAX_FRAGMENTS)
            .fragmentSize(HIGHLIGHT_FRAGMENT_SIZE)
            .highlighterType("unified")
            .boundaryScannerType(HighlightBuilder.BoundaryScannerType.SENTENCE);


    searchQuery = new NativeSearchQueryBuilder().withQuery(queryBuilder)
            .withSourceFilter(sourceFilter)
            .withHighlightBuilder(highlightBuilder)
            .withHighlightFields(new HighlightBuilder.Field("content"))
            .withFields()
            .build();

    searchQuery.setPageable(PageRequest.of(pageNumber, recordsPerPage));


    BookSearchResult books = new BookSearchResult();
    long t1 = System.currentTimeMillis();

    Page<Book> manifestacionesPage = elasticSearchTemplate.queryForPage(searchQuery, Book.class, new SearchResultMapper() {
      @Override
      public <T> AggregatedPage<T> mapResults(SearchResponse response, Class<T> clazz, Pageable pageable) {
        List<Book> chunk = new ArrayList<>();
        for (SearchHit searchHit : response.getHits()) {
          if (response.getHits().getHits().length <= 0) {
            return null;
          }
          Book book = new Book();
          book.setId(searchHit.getId());

          try {
            book = new ObjectMapper().readValue(searchHit.getSourceAsString(), Book.class);
          } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
          }
          Text[] fragmentsText = searchHit.getHighlightFields().get("content").fragments();

          String[] fragmentsString = Arrays.stream(fragmentsText)
                  .map(Object::toString)
                  .toArray(String[]::new);

          book.setHightLightFragments(fragmentsString);
          chunk.add(book);

        }
        if (chunk.size() > 0) {
          return new AggregatedPageImpl<T>((List<T>) chunk) {
          };
        }
        return null;
      }
    });

    long numResults = elasticSearchTemplate.count(searchQuery, Book.class);
    long t2 = System.currentTimeMillis();
    books.setBooks(manifestacionesPage);
    books.setNumResults(numResults);
    books.setMaxNumResults(10000); //TODO
    if (numResults > 10000) {  //TODO
      books.setNumPages((int) Math.ceil((float) 10000 / recordsPerPage)); //TODO
    } else {
      books.setNumPages((int) Math.ceil((float) numResults / recordsPerPage));
    }


    books.setMillis(t2 - t1);
    return books;
  }


}
