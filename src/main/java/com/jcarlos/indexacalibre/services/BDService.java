package com.jcarlos.indexacalibre.services;

import com.jcarlos.indexacalibre.model.Book;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Service
public class BDService {

  @Value("${calibre.path}")
  private String pathCalibre;

  private final Log log = LogFactory.getLog(BDService.class);

  public Connection getConnection() {
    try {
      return DriverManager.getConnection("jdbc:sqlite:" + pathCalibre + "metadata.db");
    } catch (SQLException e) {
      log.error("ERROR - connect()" + e);
      e.printStackTrace();
    }
    return null;
  }


  private Book getBookFromBDRow(ResultSet resultSet) throws SQLException {

    int idCalibre = resultSet.getInt("id");
    Book book = new Book();
    book.setId(""+idCalibre);
    book.setTitle(resultSet.getString("title"));
    book.setPath((resultSet.getString("path")));
    book.setAuthors(getBookAuthors(idCalibre));
    book.setTags(getBookTags(idCalibre));
    book.setContentFileName(getContentFileName(idCalibre));
    return book;
  }

  public ArrayList<Book> getAllBooks(int maxIdCalibre, int limit) {
    ArrayList<Book> books = new ArrayList<>();
    String query = "SELECT * FROM books WHERE id>" + maxIdCalibre + " ORDER BY id ASC LIMIT "+limit;
    try {
      ResultSet resultSet = getConnection().createStatement().executeQuery(query);
      while (resultSet.next()) {
        books.add(getBookFromBDRow(resultSet));
      }
    } catch (SQLException e) {
      log.error(e);
      e.printStackTrace();
    }
    return books;
  }

  public Book getBook(int id) {
    String query = "SELECT * FROM books WHERE id=" + id;
    try {
      ResultSet resultSet = getConnection().createStatement().executeQuery(query);
      if (resultSet.next()) {
        return getBookFromBDRow(resultSet);
      }
    } catch (SQLException e) {
      log.error(e);
      e.printStackTrace();
    }
    return null;
  }

  private String getContentFileName(int idCalibre) {
    String query = "SELECT name FROM data WHERE format LIKE 'EPUB' AND book=" + idCalibre;
    try {
      ResultSet resultSet = getConnection().createStatement().executeQuery(query);
      // Only first
      if (resultSet.next()) {
        String auxContentFileName=resultSet.getString("name");
        if (auxContentFileName!=null && auxContentFileName.trim().length()>0) {
          auxContentFileName+=".epub";
        }
        return auxContentFileName;
      }
    } catch (SQLException e) {
      log.error(e);
      e.printStackTrace();
    }
    return null;
  }

  private ArrayList<String> getBookAuthors(int idCalibre) {
    ArrayList<String> authors = new ArrayList<>();
    String query = "SELECT a.name FROM authors a INNER JOIN books_authors_link ba ON ba.author=a.id WHERE ba.book=" + idCalibre;
    try {
      ResultSet resultSet = getConnection().createStatement().executeQuery(query);
      while (resultSet.next()) {
        authors.add(resultSet.getString("name"));
      }
    } catch (SQLException e) {
      log.error(e);
      e.printStackTrace();
    }
    return authors;
  }


  private ArrayList<String> getBookTags(int idCalibre) {
    ArrayList<String> tags = new ArrayList<>();
    String query = "SELECT t.name FROM tags t INNER JOIN books_tags_link bt ON bt.tag=t.id WHERE bt.book=" + idCalibre;
    try {
      ResultSet resultSet = getConnection().createStatement().executeQuery(query);
      while (resultSet.next()) {
        tags.add(resultSet.getString("name"));
      }
    } catch (SQLException e) {
      log.error(e);
      e.printStackTrace();
    }

    return tags;
  }

}
