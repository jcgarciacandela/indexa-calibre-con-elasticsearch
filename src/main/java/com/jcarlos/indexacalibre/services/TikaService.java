package com.jcarlos.indexacalibre.services;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.epub.EpubParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.stereotype.Service;
import org.xml.sax.ContentHandler;

import java.io.FileInputStream;
import java.io.InputStream;

@Service
public class TikaService {
  private static final Log log = LogFactory.getLog(TikaService.class);

  public static String extractContent(String filePath) {
    try {
      InputStream inputStream=new FileInputStream(filePath);
//      Parser parser = new AutoDetectParser();
      EpubParser parser = new EpubParser();
      ContentHandler handler = new BodyContentHandler(-1);
      Metadata metadata = new Metadata();
      ParseContext context = new ParseContext();

      parser.parse(inputStream, handler, metadata, context);
      return handler.toString();

    } catch (Exception e) {
      log.error("Error extracting " + filePath);
      e.printStackTrace();
      return "";
    }
  }




}

