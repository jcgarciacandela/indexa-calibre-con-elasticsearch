package com.jcarlos.indexacalibre.services;

import com.jcarlos.indexacalibre.model.Book;
import com.jcarlos.indexacalibre.repositories.BookRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.metrics.max.Max;
import org.elasticsearch.search.aggregations.metrics.max.MaxAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class BookIndexService {
  private BookRepository bookRepository;
  private BDService bdService;
  private ElasticsearchOperations elasticSearchTemplate;

  private static final Log log = LogFactory.getLog(BookIndexService.class);

  @Value("${calibre.path}")
  private String pathCalibre;

  @Autowired
  void setBookRepository(BookRepository bookRepository) {
    this.bookRepository = bookRepository;
  }

  @Autowired
  void setBdService(BDService bdService) {
    this.bdService = bdService;
  }

  @Autowired
  void setElasticSearchTemplate(ElasticsearchOperations elasticSearchTemplate) {
    this.elasticSearchTemplate = elasticSearchTemplate;
  }

  /* Se usa para continuar la indexación masiva. Devuelve el mayor id indexado */
  private int getMaxIdIndexado() throws IOException {
    IndexRequest indexRequest = new IndexRequest("calibre");

    GetRequest getRequest=new GetRequest("calibre");

    boolean exists = elasticSearchTemplate.indexExists("calibre");

    if (!exists) return -1;
    MaxAggregationBuilder aggregation =
            AggregationBuilders
                    .max("maxIndexedId")
                    .field("idCalibre");

    MatchAllQueryBuilder matchAllQueryBuilder = QueryBuilders.matchAllQuery();

    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
            .aggregation(aggregation)
            .query(matchAllQueryBuilder);

    SearchRequest searchRequest =new SearchRequest(new String[]{"calibre"},searchSourceBuilder);

    SearchResponse searchResponse = elasticSearchTemplate.getClient().prepareSearch("calibre")
            .setQuery(QueryBuilders.matchAllQuery())
            .addAggregation(aggregation)
            .execute().actionGet();


    Max agg = searchResponse.getAggregations().get("maxIndexedId");
    return (int) Math.round(agg.getValue());
  }

  public HashMap<String, Integer> indexAllBooks(boolean continuar, int maxBooksIndexAll) throws IOException {
    int maxIdCalibreIndexado = 0;
    if (continuar) {
      maxIdCalibreIndexado = getMaxIdIndexado();
    }
    log.info("Indexing from id: " + maxIdCalibreIndexado);
    ArrayList<Book> books = bdService.getAllBooks(maxIdCalibreIndexado, maxBooksIndexAll);
    long t1=System.currentTimeMillis();
    for (Book book : books) {
      String filePath=pathCalibre+book.getPath()+ File.separator+book.getContentFileName();
      String content=TikaService.extractContent(filePath);
      book.setContent(content);
      book.setContentFileSize(new File(filePath).length());
      book.setContentSize(content.length());
      bookRepository.save(book);
      book.setContent(null); // For not logging book content
      log.info(book);
    }
    long t2=System.currentTimeMillis();
    HashMap <String, Integer> result= new HashMap<>();
    result.put("numBooksIndexed",books.size());
    result.put("firstId",maxIdCalibreIndexado);
    result.put("lastId",getMaxIdIndexado());
    result.put("millis",(int)(t2-t1));

    return result;
  }

  public Book indexBook(int id) {
    Book book=bdService.getBook(id);
    if (book.getContentFileName()!=null) {
      String filePath=pathCalibre+book.getPath()+ File.separator+book.getContentFileName();
      if (new File(filePath).exists()) {
        String content=TikaService.extractContent(filePath);
        book.setContent(content);
        book.setContentFileSize(new File(filePath).length());
        book.setContentSize(content.length());
      }
      log.info(filePath);
    }
    return bookRepository.save(book);
  }

}
