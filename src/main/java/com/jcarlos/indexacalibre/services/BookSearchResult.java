package com.jcarlos.indexacalibre.services;

import com.jcarlos.indexacalibre.model.Book;
import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class BookSearchResult
{
  private long numResults;
  int maxNumResults;
  int numPages;
  long millis;
  Page<Book> books;

}
