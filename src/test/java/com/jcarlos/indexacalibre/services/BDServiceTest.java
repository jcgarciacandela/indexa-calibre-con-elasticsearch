package com.jcarlos.indexacalibre.services;

import org.junit.jupiter.api.Test;

class BDServiceTest {

  @Test
  void testBD() throws Exception {
    assert (new BDService().getConnection() != null);
    new BDService().getConnection().createStatement().executeQuery("SELECT count(id) FROM books;");
  }
}